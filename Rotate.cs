﻿using UnityEngine;

public class Rotate : ICommand
{
    Transform t;
    float angle;

    public Rotate(Transform _t, float _angle)
    {
        this.t = _t;
        this.angle = _angle;
    }
    public void Perform()
    {
        t.Rotate(Vector3.up*angle);
    }

    public void Undo()
    {
        t.Rotate(-Vector3.up * angle);
    }
}
