﻿using UnityEngine;

public class MoveForward : ICommand
{


    Transform t;
    float dist;

    public MoveForward(Transform _t, float _dist)
    {
        this.t = _t;
        this.dist = _dist;
    }
    public void Perform()
    {
        t.position = t.position + Vector3.up * dist;
    }

    public void Undo()
    {
        t.position = t.position - Vector3.up * dist;
    }
}
