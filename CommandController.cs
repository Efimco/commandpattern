﻿using UnityEngine;
using UnityEngine.Events;

public class CommandController : MonoBehaviour
{

    private CommandInvoker Invoker = new CommandInvoker();




    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            var Move = new MoveForward(transform, 1);
            Invoker.onAddCommand.Invoke(Move);
            Debug.Log("Command Added");
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Invoker.Process();
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            Invoker.ProcessAll();
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            Invoker.Undo();
            Debug.Log("Command Removed");
        }

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log(Invoker.GetCount());
        }
    }


}
