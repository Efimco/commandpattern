﻿using System.Collections.Generic;
using UnityEngine.Events;

[System.Serializable]
public class OnAddCommand : UnityEvent<ICommand>
{

}
public class CommandInvoker
{
    private List<ICommand> commands;
    private List<ICommand> performedCommands;
    public OnAddCommand onAddCommand;
    public UnityEvent onProcessAll;

    public CommandInvoker()
    {
        commands = new List<ICommand>();
        performedCommands = new List<ICommand>();

        onAddCommand = new OnAddCommand();
        onAddCommand.AddListener(AddCommand);
        onProcessAll = new UnityEvent();
        onProcessAll.AddListener(ProcessAll);



    }
    private void AddCommand(ICommand command)
    {
        commands.Add(command);

    }

    private void ProcessAll()
    {
        if (commands[0] is ICommand)
        {
            for (int i = 0; i < commands.Count; i++)
            {
                commands[i].Perform();
                performedCommands.Add(commands[i]);
                commands.RemoveAt(i);
            }
        }
    }
    private void Process()
    {
        if (commands[0] is ICommand)
        {
            commands[0].Perform();
            performedCommands.Add(commands[0]);
            commands.Remove(commands[0]);
        }
    }
    private void Undo()
    {
        if (performedCommands[0] is ICommand)
        {
            performedCommands[0].Undo();
            performedCommands.Remove(performedCommands[0]);
        }

    }
    private void ClearQueue()
    {
        if (commands[0] is ICommand)
        {
            commands.Clear();
        }
    }

    public string GetCount()
    {
        return $"Queue: {commands.Count} |||| Performed Commands: {performedCommands.Count}";
    }
}
