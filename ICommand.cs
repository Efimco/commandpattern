﻿public interface ICommand
{

    void Perform();
    void Undo();
}
